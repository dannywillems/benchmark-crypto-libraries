use bls12_381::Scalar;
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use ff::Field;
use rand;

fn add_fr(a: Scalar, b: Scalar) -> Scalar {
    a + b
}

fn mul_fr(a: Scalar, b: Scalar) -> Scalar {
    a * b
}

fn criterion_benchmark(c: &mut Criterion) {
    c.bench_function("Scalar BLS12-381 add ", |bench| {
        let rng = &mut rand::thread_rng();
        let a = Scalar::random(rng);
        let rng = &mut rand::thread_rng();
        let b = Scalar::random(rng);
        bench.iter(|| add_fr(a, b));
    });

    c.bench_function("Scalar BLS12-381 mul", |bench| {
        let rng = &mut rand::thread_rng();
        let a = Scalar::random(rng);
        let rng = &mut rand::thread_rng();
        let b = Scalar::random(rng);
        bench.iter(|| mul_fr(a, b));
    });
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
