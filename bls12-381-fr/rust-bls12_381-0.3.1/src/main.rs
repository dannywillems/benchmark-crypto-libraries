use bls12_381::Scalar;
use ff::Field;
use rand;
use std::time::SystemTime;

fn main() {
    let rng = &mut rand::thread_rng();
    let a = Scalar::random(rng);
    let rng = &mut rand::thread_rng();
    let b = Scalar::random(rng);
    let start_time = SystemTime::now();
    let _c = a + b;
    let end_time = SystemTime::now();
    println!("Addition: {:?}", end_time.duration_since(start_time));
    let start_time = SystemTime::now();
    let _c = a * b;
    let end_time = SystemTime::now();
    println!("Multiplication: {:?}", end_time.duration_since(start_time));
    let start_time = SystemTime::now();
    a.invert();
    let end_time = SystemTime::now();
    println!("Inverse: {:?}", end_time.duration_since(start_time));
    let start_time = SystemTime::now();
    a.neg();
    let end_time = SystemTime::now();
    println!("Negation: {:?}", end_time.duration_since(start_time));
}
