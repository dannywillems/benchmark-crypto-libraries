#![feature(test)]

extern crate test;
use test::Bencher;

use bls12_381::Scalar;
use ff::Field;
use rand;
use std::time::SystemTime;

fn main() {
    let rng = &mut rand::thread_rng();
    let a = Scalar::random(rng);
    let rng = &mut rand::thread_rng();
    let b = Scalar::random(rng);
    let start_time = SystemTime::now();
    let _c = a.add(&b);
    let end_time = SystemTime::now();
    println!("Addition: {:?}", end_time.duration_since(start_time));
}

// #[bench]
// fn bench_add(bench: &mut Bencher) {
//     let rng = &mut rand::thread_rng();
//     let a = Scalar::random(rng);
//     let rng = &mut rand::thread_rng();
//     let b = Scalar::random(rng);

//     bench.iter(|| {
//         let c = a + b;
//         // c == Scalar::zero()
//     });
// }
