use dusk_plonk::bls12_381::BlsScalar;
use rand;
use std::time::SystemTime;

fn main() {
    let rng = &mut rand::thread_rng();
    let a = BlsScalar::random(rng);
    let b = BlsScalar::random(rng);
    let start_time = SystemTime::now();
    a + b;
    let end_time = SystemTime::now();
    println!("Addition: {:?}", end_time.duration_since(start_time));
    let start_time = SystemTime::now();
    a * b;
    let end_time = SystemTime::now();
    println!("Multiplication: {:?}", end_time.duration_since(start_time));
    let start_time = SystemTime::now();
    a.invert();
    let end_time = SystemTime::now();
    println!("Inverse: {:?}", end_time.duration_since(start_time));
    let start_time = SystemTime::now();
    a.neg();
    let end_time = SystemTime::now();
    println!("Negation: {:?}", end_time.duration_since(start_time));
}
