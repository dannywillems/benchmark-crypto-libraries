## Result for BLS12-381 Fr

```
Estimated testing time 4m (24 benchmarks x 10s). Change using '-quota'.
┌───────────────────────────────────────────────────────┬─────────────┬─────────┬────────────┐
│ Name                                                  │    Time/Run │ mWd/Run │ Percentage │
├───────────────────────────────────────────────────────┼─────────────┼─────────┼────────────┤
│ Fr compute addition pregenerated random element       │    198.77ns │  18.00w │      1.93% │
│ Fr random generation                                  │ 10_294.44ns │  10.00w │    100.00% │
│ Fr zero generation                                    │      2.31ns │         │      0.02% │
│ Fr one generation                                     │      2.30ns │         │      0.02% │
│ Fr check if zero on pregenerated random               │     69.69ns │   4.00w │      0.68% │
│ Fr check if zero on pregenerated one                  │     69.41ns │   4.00w │      0.67% │
│ Fr check if zero on pregenerated zero                 │     71.30ns │   4.00w │      0.69% │
│ Fr check if one on pregenerated random                │     72.03ns │   4.00w │      0.70% │
│ Fr check if one on pregenerated one                   │     71.81ns │   4.00w │      0.70% │
│ Fr check if one on pregenerated zero                  │     71.62ns │   4.00w │      0.70% │
│ Fr compute addition on pregenerate random             │    199.90ns │  18.00w │      1.94% │
│ Fr compute multiplication on pregenerate random       │    234.27ns │  18.00w │      2.28% │
│ Fr compute square on pregenerate random               │    171.11ns │  14.00w │      1.66% │
│ Fr compute double on pregenerate random               │    136.73ns │  14.00w │      1.33% │
│ Fr compute equality on random                         │    131.29ns │   8.00w │      1.28% │
│ Fr compute equality on same element                   │    132.23ns │   8.00w │      1.28% │
│ Fr compute opposite of pregenerated random element    │    141.54ns │  14.00w │      1.37% │
│ Fr compute opposite of pregenerated one element       │    142.38ns │  14.00w │      1.38% │
│ Fr compute opposite of pregenerated zero element      │    138.72ns │  14.00w │      1.35% │
│ Fr compute inverse of pregenerated random element     │  3_796.97ns │  14.00w │     36.88% │
│ Fr compute inverse of pregenerated one element        │  3_782.02ns │  14.00w │     36.74% │
│ Fr compute inverse opt of pregenerated random element │  3_904.15ns │  20.00w │     37.92% │
│ Fr compute inverse opt of pregenerated one element    │  3_883.24ns │  20.00w │     37.72% │
│ Fr compute inverse opt of pregenerated zero element   │     73.74ns │   4.00w │      0.72% │
└───────────────────────────────────────────────────────┴─────────────┴─────────┴────────────┘
```
