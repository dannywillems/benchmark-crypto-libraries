module BenchmarkFr = Ff_bench.MakeBench (Bls12_381__Fr)

let () =
  let commands =
    List.concat [BenchmarkFr.get_benches "Fr from ocaml-bls12-381"]
  in
  Core.Command.run (Core_bench.Bench.make_command commands)
