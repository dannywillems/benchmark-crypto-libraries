use ff::Field;
use pairing::bls12_381;
use pairing::bls12_381::Fr;

use rand::rngs::OsRng;
use std::time::SystemTime;

fn main() {
    let mut a = Fr::random(&mut OsRng);
    let b = Fr::random(&mut OsRng);
    let start_time = SystemTime::now();
    a.add_assign(&b);
    let end_time = SystemTime::now();
    println!("Addition: {:?}", end_time.duration_since(start_time));
    let mut a = Fr::random(&mut OsRng);
    let b = Fr::random(&mut OsRng);
    let start_time = SystemTime::now();
    a.mul_assign(&b);
    let end_time = SystemTime::now();
    println!("Multiplication: {:?}", end_time.duration_since(start_time));
}
