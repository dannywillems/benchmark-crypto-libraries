#[macro_use]
extern crate criterion;

use ark_std::test_rng;
use criterion::{black_box, Criterion};
use std::ops::Add;
use std::ops::Mul;
use std::ops::Neg;
use std::ops::Sub;

use ark_ff::fields::{Field, Fp256, MontBackend, MontConfig};
use ark_ff::UniformRand;

#[derive(MontConfig)]
#[modulus = "52435875175126190479447740508185965837690552500527637822603658699938581184513"]
#[generator = "7"]
pub struct FrConfig;
pub type Fr = Fp256<MontBackend<FrConfig, 4>>;

fn add(c: &mut Criterion) {
    let mut rng = test_rng();
    let x = Fr::rand(&mut rng);
    let y = Fr::rand(&mut rng);
    c.bench_function("BLS12-381 Fr: add", |b| {
        b.iter(|| {
            let r = Fr::add(black_box(x), black_box(y));
            r
        })
    });
}

fn sub(c: &mut Criterion) {
    let mut rng = test_rng();
    let x = Fr::rand(&mut rng);
    let y = Fr::rand(&mut rng);
    c.bench_function("BLS12-381 Fr: sub", |b| {
        b.iter(|| {
            let r = Fr::sub(black_box(x), black_box(y));
            r
        })
    });
}

fn mul(c: &mut Criterion) {
    let mut rng = test_rng();
    let x = Fr::rand(&mut rng);
    let y = Fr::rand(&mut rng);
    c.bench_function("BLS12-381 Fr: mul", |b| {
        b.iter(|| {
            let r = Fr::mul(black_box(x), black_box(y));
            r
        })
    });
}

fn negate(c: &mut Criterion) {
    let mut rng = test_rng();
    let x = Fr::rand(&mut rng);
    c.bench_function("BLS12-381 Fr: negate", |b| b.iter(|| Fr::neg(black_box(x))));
}

fn inverse(c: &mut Criterion) {
    let mut rng = test_rng();
    let x = Fr::rand(&mut rng);
    c.bench_function("BLS12-381 Fr: inverse", |b| {
        b.iter(|| Fr::inverse(black_box(&x)))
    });
}

fn square(c: &mut Criterion) {
    let mut rng = test_rng();
    let x = Fr::rand(&mut rng);
    c.bench_function("BLS12-381 Fr: square", |b| {
        b.iter(|| Fr::square(black_box(&x)))
    });
}

fn bench_scalar_field(c: &mut Criterion) {
    add(c);
    sub(c);
    mul(c);
    negate(c);
    inverse(c);
    square(c);
}

criterion_group!(benches, bench_scalar_field);
criterion_main!(benches);
