## Build instructions

With assembly code:
```
cargo +nightly bench --features asm
```

Without assembly code:
```
cargo +nightly bench
```
