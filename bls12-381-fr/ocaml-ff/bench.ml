module Fr = Ff.MakeFp (struct
  let prime_order =
    Z.of_string
      "52435875175126190479447740508185965837690552500527637822603658699938581184513"
end)

module BenchmarkFr = Ff_bench.MakeBench (Fr)

let () =
  let commands = List.concat [BenchmarkFr.get_benches "Fr from ocaml-ff"] in
  Core.Command.run (Core_bench.Bench.make_command commands)
