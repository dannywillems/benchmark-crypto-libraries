## Result for ocaml-ff

```
ocaml-ff git:(master) ✗ dune exec ./bench.exe
Info: Creating file dune-project with this contents:
| (lang dune 3.0)
Estimated testing time 4m (24 benchmarks x 10s). Change using '-quota'.
┌─────────────────────────────────────────────────────────────────────┬────────────┬─────────┬────────────┐
│ Name                                                                │   Time/Run │ mWd/Run │ Percentage │
├─────────────────────────────────────────────────────────────────────┼────────────┼─────────┼────────────┤
│ Fr from ocaml-ff compute addition pregenerated random element       │   125.91ns │  22.00w │      7.65% │
│ Fr from ocaml-ff random generation                                  │ 1_117.62ns │  33.00w │     67.92% │
│ Fr from ocaml-ff zero generation                                    │     2.31ns │         │      0.14% │
│ Fr from ocaml-ff one generation                                     │     2.29ns │         │      0.14% │
│ Fr from ocaml-ff check if zero on pregenerated random               │    89.32ns │  14.00w │      5.43% │
│ Fr from ocaml-ff check if zero on pregenerated one                  │    34.03ns │   3.00w │      2.07% │
│ Fr from ocaml-ff check if zero on pregenerated zero                 │    35.84ns │   3.00w │      2.18% │
│ Fr from ocaml-ff check if one on pregenerated random                │    85.62ns │  14.00w │      5.20% │
│ Fr from ocaml-ff check if one on pregenerated one                   │    35.59ns │   3.00w │      2.16% │
│ Fr from ocaml-ff check if one on pregenerated zero                  │    35.92ns │   3.00w │      2.18% │
│ Fr from ocaml-ff compute addition on pregenerate random             │   116.39ns │  22.00w │      7.07% │
│ Fr from ocaml-ff compute multiplication on pregenerate random       │   224.90ns │  29.00w │     13.67% │
│ Fr from ocaml-ff compute square on pregenerate random               │   217.15ns │  29.00w │     13.20% │
│ Fr from ocaml-ff compute double on pregenerate random               │    37.70ns │   8.00w │      2.29% │
│ Fr from ocaml-ff compute equality on random                         │   159.19ns │  28.00w │      9.67% │
│ Fr from ocaml-ff compute equality on same element                   │   167.90ns │  28.00w │     10.20% │
│ Fr from ocaml-ff compute opposite of pregenerated random element    │    35.56ns │   8.00w │      2.16% │
│ Fr from ocaml-ff compute opposite of pregenerated one element       │    38.97ns │   7.00w │      2.37% │
│ Fr from ocaml-ff compute opposite of pregenerated zero element      │    11.43ns │         │      0.69% │
│ Fr from ocaml-ff compute inverse of pregenerated random element     │ 1_645.37ns │   7.00w │    100.00% │
│ Fr from ocaml-ff compute inverse of pregenerated one element        │   227.59ns │   4.00w │     13.83% │
│ Fr from ocaml-ff compute inverse opt of pregenerated random element │ 1_643.96ns │   9.00w │     99.91% │
│ Fr from ocaml-ff compute inverse opt of pregenerated one element    │   214.22ns │   6.00w │     13.02% │
│ Fr from ocaml-ff compute inverse opt of pregenerated zero element   │   155.97ns │         │      9.48% │
└─────────────────────────────────────────────────────────────────────┴────────────┴─────────┴────────────┘
```
