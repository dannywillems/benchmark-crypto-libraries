#[macro_use]
extern crate criterion;

use criterion::{black_box, Criterion};
use rand::thread_rng;
use rand::RngCore;

use blst::*;

fn add(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut res: blst_fr = blst_fr { l: [0; 4] };
    let mut buffer = [0u8; 32];
    let mut scalar: blst_scalar = blst_scalar { b: [0; 32] };

    rng.fill_bytes(&mut buffer);
    let mut x: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut x, &scalar) };

    rng.fill_bytes(&mut buffer);
    let mut y: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut y, &scalar) };

    c.bench_function("Add", |b| {
        b.iter(|| unsafe { blst_fr_add(black_box(&mut res), black_box(&x), black_box(&y)) })
    });
}

fn sub(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut res: blst_fr = blst_fr { l: [0; 4] };
    let mut buffer = [0u8; 32];
    let mut scalar: blst_scalar = blst_scalar { b: [0; 32] };

    rng.fill_bytes(&mut buffer);
    let mut x: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut x, &scalar) };

    rng.fill_bytes(&mut buffer);
    let mut y: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut y, &scalar) };

    c.bench_function("Sub", |b| {
        b.iter(|| unsafe { blst_fr_sub(black_box(&mut res), black_box(&x), black_box(&y)) })
    });
}

fn mul(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut res: blst_fr = blst_fr { l: [0; 4] };
    let mut buffer = [0u8; 32];
    let mut scalar: blst_scalar = blst_scalar { b: [0; 32] };

    rng.fill_bytes(&mut buffer);
    let mut x: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut x, &scalar) };

    rng.fill_bytes(&mut buffer);
    let mut y: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut y, &scalar) };

    c.bench_function("Mul", |b| {
        b.iter(|| unsafe { blst_fr_mul(black_box(&mut res), black_box(&x), black_box(&y)) })
    });
}

fn square(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut buffer = [0u8; 32];
    rng.fill_bytes(&mut buffer);
    let mut scalar: blst_scalar = blst_scalar { b: [0; 32] };
    let mut x: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut x, &scalar) };

    c.bench_function("Square", |b| {
        b.iter(|| unsafe { blst_fr_sqr(black_box(&mut x), black_box(&x)) })
    });
}

fn negate(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut buffer = [0u8; 32];
    rng.fill_bytes(&mut buffer);
    let mut scalar: blst_scalar = blst_scalar { b: [0; 32] };
    let mut x: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut x, &scalar) };

    c.bench_function("Negate", |b| {
        b.iter(|| unsafe { blst_fr_cneg(black_box(&mut x), black_box(&x), true) })
    });
}

fn inverse(c: &mut Criterion) {
    let mut rng = thread_rng();
    let mut buffer = [0u8; 32];
    rng.fill_bytes(&mut buffer);
    let mut scalar: blst_scalar = blst_scalar { b: [0; 32] };
    let mut x: blst_fr = blst_fr { l: [0; 4] };
    unsafe { blst_scalar_from_le_bytes(&mut scalar, &buffer as *const u8, 32) };
    unsafe { blst_fr_from_scalar(&mut x, &scalar) };

    c.bench_function("Inverse", |b| {
        b.iter(|| unsafe { blst_fr_inverse(black_box(&mut x), black_box(&x)) })
    });
}

fn bench_scalar_field(c: &mut Criterion) {
    add(c);
    sub(c);
    mul(c);
    inverse(c);
    negate(c);
    square(c);
}

criterion_group!(benches, bench_scalar_field);
criterion_main!(benches);
