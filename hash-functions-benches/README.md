# Benchmark hash functions

This directory benches different hash functions for the same input size.
To run:

```
opam switch create 4.14.1 ./
opam pin add bls12-381-hash.2.0.0 git+https://gitlab.com/nomadic-labs/cryptography/ocaml-bls12-381-hash\#2.0.0-rc.0
opam install core_bench bls12-381-hash.2.0.0 bls12-381.6.0.1 core tezos-crypto ocamlformat.0.24.1 merlin
dune exec ./src/bench.exe -- -q 2
```

Output example:
```
┌─────────────────────────────────────────────────────────────────────────┬──────────────┬─────────┬────────────┐
│ Name                                                                    │     Time/Run │ mWd/Run │ Percentage │
├─────────────────────────────────────────────────────────────────────────┼──────────────┼─────────┼────────────┤
│ Hash anemoi on 2 Fr elements (size in bytes = 64, size in bits = 512)   │ 120_605.14ns │  58.00w │    100.00% │
│ Hash Poseidon on 3 Fr elements (size in bytes = 96, size in bits = 768) │  17_630.50ns │         │     14.62% │
│ Hash Blake2b on input size in bytes = 64, size in bits = 512            │     314.39ns │  27.00w │      0.26% │
└─────────────────────────────────────────────────────────────────────────┴──────────────┴─────────┴────────────┘
```
