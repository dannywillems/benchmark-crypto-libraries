use ark_crypto_primitives::crh::{poseidon::CRH, CRHSchemeGadget};
use ark_crypto_primitives::CRHScheme;
use ark_ff::PrimeField;
use ark_r1cs_std::alloc::{AllocVar, AllocationMode};
use ark_r1cs_std::fields::fp::FpVar;
use ark_r1cs_std::R1CSVar;
use ark_relations::r1cs::{Namespace, SynthesisError};
use ark_sponge::constraints::CryptographicSpongeVar;
use ark_sponge::poseidon::constraints::PoseidonSpongeVar;
use ark_sponge::Absorb;
use ark_std::borrow::Borrow;
use ark_std::marker::PhantomData;

use ark_bls12_381::Fr;
use ark_r1cs_std::fields::fp::AllocatedFp;
use ark_relations::r1cs::ConstraintSystem;
use ark_sponge::poseidon::PoseidonParameters;
use ark_std::UniformRand;

#[derive(Clone)]
pub struct CRHParametersVar<F: PrimeField + Absorb> {
    pub parameters: PoseidonParameters<F>,
}

pub struct CRHGadget<F: PrimeField + Absorb> {
    field_phantom: PhantomData<F>,
}

impl<F: PrimeField + Absorb> CRHSchemeGadget<CRH<F>, F> for CRHGadget<F> {
    type InputVar = [FpVar<F>];
    type OutputVar = FpVar<F>;
    type ParametersVar = CRHParametersVar<F>;

    fn evaluate(
        parameters: &Self::ParametersVar,
        input: &Self::InputVar,
    ) -> Result<Self::OutputVar, SynthesisError> {
        let cs = input.cs();

        if cs.is_none() {
            let mut constant_input = Vec::new();
            for var in input.iter() {
                constant_input.push(var.value()?);
            }
            Ok(FpVar::Constant(
                CRH::<F>::evaluate(&parameters.parameters, constant_input).unwrap(),
            ))
        } else {
            let mut sponge = PoseidonSpongeVar::new(cs, &parameters.parameters);
            sponge.absorb(&input)?;
            let res = sponge.squeeze_field_elements(1)?;
            Ok(res[0].clone())
        }
    }
}

impl<F: PrimeField + Absorb> AllocVar<PoseidonParameters<F>, F> for CRHParametersVar<F> {
    fn new_variable<T: Borrow<PoseidonParameters<F>>>(
        _cs: impl Into<Namespace<F>>,
        f: impl FnOnce() -> Result<T, SynthesisError>,
        _mode: AllocationMode,
    ) -> Result<Self, SynthesisError> {
        f().and_then(|param| {
            let parameters = param.borrow().clone();

            Ok(Self { parameters })
        })
    }
}

fn main() {
    let mut test_rng = ark_std::test_rng();

    let r_p = 56;
    let r_f = 8;
    let width = 3;
    let alpha = 5;

    let mut mds = vec![vec![]; width];
    for i in 0..width {
        for _ in 0..width {
            mds[i].push(Fr::rand(&mut test_rng));
        }
    }

    let mut ark = vec![vec![]; r_p + r_f];
    for i in 0..r_p + r_f {
        for _ in 0..width {
            ark[i].push(Fr::rand(&mut test_rng));
        }
    }

    let mut input = Vec::new();
    for _ in 0..width {
        input.push(Fr::rand(&mut test_rng));
    }

    let cs = ConstraintSystem::<Fr>::new_ref();

    let mut input_var = Vec::new();
    for elem in input.iter() {
        input_var.push(FpVar::Var(
            AllocatedFp::<Fr>::new_witness(cs.clone(), || Ok(elem)).unwrap(),
        ));
    }

    let params = PoseidonParameters::<Fr>::new(r_f as u32, r_p as u32, alpha, mds, ark);
    let params_var = CRHParametersVar::<Fr>::new_witness(cs, || Ok(params)).unwrap();
    let res_circuit = CRHGadget::<Fr>::evaluate(&params_var, &input_var).unwrap();
    println!(
        "Poseidon: number of constrains = {}, number of public variables: {}, num of private variables: {}",
        res_circuit.borrow().cs().num_constraints(),
        res_circuit.borrow().cs().num_instance_variables(),
        res_circuit.borrow().cs().num_witness_variables()
    );
}
