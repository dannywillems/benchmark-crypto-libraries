use dusk_bls12_381::BlsScalar;
use dusk_poseidon::sponge;
use rand;
use std::time::SystemTime;

fn main() {
    let n = 5;
    let mut scalars = vec![];
    for _i in 0..n {
        scalars.push(BlsScalar::random(&mut rand::thread_rng()))
    }
    let start_time = SystemTime::now();
    let _x = sponge::hash(&scalars[0..5]);
    let end_time = SystemTime::now();
    println!("Time: {:?}", end_time.duration_since(start_time));
}
