let get_time () = Sys.time ()

let run_bench description generator power (module Fr: Ff_sig.PRIME) =
  let module Poly = Polynomial.MakeUnivariate(Fr) in
  let points = List.init power (fun _i -> Fr.random ()) in
  let domain =
    Polynomial.generate_evaluation_domain (module Fr) power (Fr.of_z generator)
  in
  let start_time = get_time () in
  ignore @@ Poly.interpolation_fft ~domain points;
  let end_time = get_time () in
  Printf.printf "Interpolation FFT bench with generator of order %d %s: %fs\n" power description (end_time -. start_time)

let () =
 let generator = Z.of_string "45578933624873246016802258050230213493140367389966312656957679049059636081617" in
 (* let generator = Z.of_string "15076889834420168339092859836519192632846122361203618639585008852351569017005" in *)
 let module Fr = Ff.MakeFp (struct
  let prime_order =
    Z.of_string
      "52435875175126190479447740508185965837690552500527637822603658699938581184513"
  end) in
 run_bench "using Fr generated by ocaml-ff" generator (1 lsl 16) (module Fr);
 run_bench "using Fr generated by ocaml-bls12-381" generator (1 lsl 16) (module Bls12_381__Fr)
