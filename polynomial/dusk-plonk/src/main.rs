use dusk_plonk::bls12_381::BlsScalar;
use dusk_plonk::fft;
use rand;
use std::time::SystemTime;

use std::convert::TryInto;

fn main() {
    let rng = &mut rand::thread_rng();
    // let polynomial = fft::Polynomial::rand(1 << 16, rng);
    let points: Vec<BlsScalar> = (0..1 << 16).map(|_| BlsScalar::random(rng)).collect();
    let points: [BlsScalar; 1 << 16] = points.try_into().unwrap();
    let start_time = SystemTime::now();
    let domain_evaluation = fft::EvaluationDomain::new(1 << 16).unwrap();
    println!("{:?}", domain_evaluation.size);
    println!("{:?}", domain_evaluation.group_gen.to_bits());
    println!(
        "{:?}",
        domain_evaluation
            .group_gen
            .pow(&[domain_evaluation.size, 0, 0, 0])
    );
    let fft_result = domain_evaluation.fft(&points);
    let end_time = SystemTime::now();
    // println!("{:?}", fft_result);
    println!("{:?}", end_time.duration_since(start_time));
}
