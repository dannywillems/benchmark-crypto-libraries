use dusk_plonk::prelude::*;
use dusk_poseidon::sponge::*;
use std::time::SystemTime;

const CAPACITY: usize = 1 << 12;

fn poseidon_sponge_params<const N: usize>() -> ([BlsScalar; N], BlsScalar) {
    let mut input = [BlsScalar::zero(); N];
    input
        .iter_mut()
        .for_each(|s| *s = BlsScalar::random(&mut rand::thread_rng()));
    let output = hash(&input);
    (input, output)
}

// Checks that the result of the hades permutation is the same as the one
// obtained by the sponge gadget
fn sponge_gadget_tester<const N: usize>(
    i: &[BlsScalar],
    out: BlsScalar,
    composer: &mut StandardComposer,
) {
    let zero = composer.add_input(BlsScalar::zero());
    composer.constrain_to_constant(zero, BlsScalar::zero(), None);

    let mut i_var = vec![zero; N];
    i.iter().zip(i_var.iter_mut()).for_each(|(i, v)| {
        *v = composer.add_input(*i);
    });

    let o_var = composer.add_input(out);

    // Apply Poseidon Sponge hash to the inputs
    let computed_o_var = gadget(composer, &i_var);

    // Check that the Gadget sponge hash result = Scalar sponge hash result
    composer.add_gate(
        o_var,
        computed_o_var,
        zero,
        BlsScalar::one(),
        -BlsScalar::one(),
        BlsScalar::zero(),
        BlsScalar::zero(),
        None,
    );
    println!("Circuit properties: size = {:?}", composer.circuit_size())
}

fn main() {
    // Setup OG params.
    let public_parameters = PublicParameters::setup(CAPACITY, &mut rand::thread_rng()).unwrap();
    let (ck, vk) = public_parameters.trim(CAPACITY).unwrap();

    // Test with width = 3

    // Proving
    let (i, o) = poseidon_sponge_params::<3>();

    let mut prover = Prover::new(b"sponge_tester");
    sponge_gadget_tester::<3>(&i, o, prover.mut_cs());
    prover.preprocess(&ck);
    let start_time = SystemTime::now();
    let proof = prover.prove(&ck).unwrap();
    let end_time = SystemTime::now();
    println!("Time: {:?}", end_time.duration_since(start_time));

    // Verify
    let mut verifier = Verifier::new(b"sponge_tester");
    sponge_gadget_tester::<3>(&i, o, verifier.mut_cs());
    verifier.preprocess(&ck).unwrap();
    verifier.verify(&proof, &vk, &vec![BlsScalar::zero()]);
    ()
}
